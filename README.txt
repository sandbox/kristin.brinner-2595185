CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The user management module was built to take some of the pressure off
site administrators who are currently manually approving new users.
Sometimes this is necessary if a combination of Mollom and email
authentication or other anti-spam techniques filters out the spambots,
but not malicious users (who are real people) who want access
to forums so they can troll or post advertisements.

The basic idea is that a new user registers for an account,
answers a question about themselves (this question will be used by
trusted users to determine if the user is legit or not) and then
the user is left in the blocked state.
When trusted users vote the blocked user up or down, they
are either promoted to 'active' status or removed from the approval queue.

REQUIREMENTS
------------

None.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------

* Configure user permissions in Administration >> People >> Permissions.

- This allows you to set what roles are allowed to vote on whether
  or not blocked users should be approved (and set to active) or
  removed from the approval queue.

* Settings for this module are at
  Administration >> People >> User Moderation >> Settings

- Customize the number of votes required to approve the
  user or remove them from the approval queue.

- Select which field in the user profile form you want to display
  to voters to help them decide if the user is legit or not.

- Set the 'thank you' message once a user has voted on a user
  in the approval queue.

* Administer and view users waiting in the approval queue at
  Administration >> People >> User Moderation

MAINTAINERS
-----------

Current maintainers:
* Kristin Brinner (kbrinner) - https://www.drupal.org/u/kbrinner

Sponsored by:
* LevelTen Interactive - https://www.drupal.org/marketplace/levelten
