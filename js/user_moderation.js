/**
 * @file
 * Provides general enhancements.
 */

var Drupal = Drupal || {};

(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.userModerationFixes = {
    attach: function (context) {

      // Set jQuery object.
      var $userModBlock = $(".block-user-moderation", context);

      // Hide block if cookie is set.
      if ($.cookie('VOTED')) {
        $userModBlock.hide();
      }

      // Hide contents of block on clicking voting links.
      // Also set cookie.
      $('.user-mod-wrapper a', $userModBlock).click(function () {
        $.cookie('VOTED', 1);
        $('.user-mod-wrapper', $userModBlock).slideUp();
        $('.block-title', $userModBlock).html('Thanks for voting');
      });
    }
  };

})(jQuery, Drupal);
