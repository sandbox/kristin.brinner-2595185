<?php

/**
 * @file
 * Administration page callbacks for the user moderation module.
 */

/**
 * Implements hook_settings().
 *
 * Sets the cutoff value above which users are removed from the approval queue.
 * Also selects which fields to display on voting form.
 */
function user_moderation_settings($form, &$form_state) {

  // Get list of fields in user profile.
  // Returns all instances for user entity, keyed by bundle name.
  $field_info_instances = field_info_instances('user');

  // Make array that contains all of the custom user field arrays.
  $user_fields = $field_info_instances['user'];

  // Make array where key => value is
  // field_machine_name => field as displayed to user.
  $fields = array();
  foreach ($user_fields as $key1 => $value1) {
    $fields[$key1] = $user_fields[$key1]['label'];
  }

  $form['user_moderation_set_cutoff'] = array(
    '#type' => 'textfield',
    '#title' => t('Cutoff'),
    '#description' => t('<p>Set the cutoff above which the user is removed from the approval queue.
    For example, if you set this to 3, once a user gets 3 upvotes or downvotes they will be removed
    from the approval queue.<br/>Decreasing this number will not automatically remove users
    who already have accumulated more votes than the new cutoff value. The user will be removed once they
    are voted on again. Likewise increasing this number will not add users back into this list if they
    have already been removed from the approval queue.</p>'),
    '#default_value' => variable_get('user_moderation_set_cutoff', '3'),
  );
  // @todo i think we should make this just one field here rather than checkboxes
  $form['user_moderation_custom_fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User field(s)'),
    '#options' => $fields,
    '#description' => t('<p>Select the fields that you want to display on the voting screen to help people decide if users are real or spammers.</p>'),
    '#default_value' => variable_get('user_moderation_custom_fields', array()),
  );
  $form['user_moderation_thank_you'] = array(
    '#type' => 'textarea',
    '#title' => t('Thank you message'),
    '#description' => t('<p>Enter the message you want to display to users who will be voting explaining what you are doing here.</p>
    <p>All set? <a href="admin/people/user-moderation">Head back to the moderation page</a></p>'),
    '#default_value' => variable_get('user_moderation_thank_you', array()),
  );

  return system_settings_form($form);
}

/**
 * Validate that cutoff value for user moderation is an integer.
 */
function user_moderation_settings_validate($form, &$form_state) {
  $cutoff = $form_state['values']['user_moderation_set_cutoff'];
  if (!is_numeric($cutoff) || ($cutoff <= 0)) {
    form_set_error('user_moderation_set_cutoff', t('Please enter number greater than 0.'));
  };
}

/**
 * Build the user moderation administrative table.
 *
 * @return array
 *   A render array set for theming by theme_table().
 */
function user_moderation_approval_page() {

  // Make sure Drupal Ajax framework javascript
  // and jQuery cookie library are included.
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'jquery.cookie');

  // Get variables for the number of votes required for a user to be approved
  // or removed from the approval queue, as well as fields selected for display
  // on voting screen.
  $cutoff = variable_get('user_moderation_set_cutoff');
  // @todo this is being repeated in .module, how to get it all in one place
  $fields = array_filter(variable_get('user_moderation_custom_fields', array()));

  // Print out the current value of the cutoff variable as well as a link
  // to modify this value.
  // @todo should move all of this $output .= into renderable array.
  $output = '<p>Currently this module is set to remove users from the approval
  queue if they have more than ' . $cutoff . ' votes (either up or down).<br/><a href="/admin/people/user-moderation/cutoff">Change this value.</a></p>';

  // Print out the fields the user has selected to display on the voting form.
  $output .= '<p>You have selected the following fields to display on the voting form: ';

  // Loop through variables array where user has
  // selected which user profile fields to display.
  foreach ($fields as $key => $value) {
    $field_name = $fields[$key];
    $output .= '<br/>' . $field_name;
  };

  $output .= '<br/><a href="/admin/people/user-moderation/cutoff">Modify or add more fields.</a></p>';

  // Print out the message to users asking them to vote,
  $thanks = variable_get('user_moderation_thank_you');
  $output .= '<p>This is the message currently displaying to users asking them to vote: ' . $thanks;
  $output .= '<br/><a href="/admin/people/user-moderation/cutoff">Modify this message.</a></p>';

  // We are going to output the results in a table with a nice header.
  $header = array(
    // The header gives the table the information it needs in order to make
    // the query calls for ordering. TableSort uses the field information
    // to know what database column to sort by.
    'uid' => array('data' => t('User ID'), 'field' => 'um.uid'),
    'upvotes' => array('data' => t('Upvotes'), 'field' => 'um.upvotes'),
    'downvotes' => array('data' => t('Downvotes'), 'field' => 'um.downvotes'),
    // We will add our 'downvote' and 'upvote' links
    // as operations in this last column.
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
    // We will get this field from {field_data_field_about_yourself}
    'tell' => array('data' => t('User Information')),
    'edit' => array('data' => t('Edit User')),
  );

  // Initialize $query with db_select().
  $query = db_select('user_moderation', 'um');
  // Using the TableSort Extender is what tells the the query object that we
  // are sorting.
  $query = $query->extend('TableSort');
  // Add the pager.
  $query = $query->extend('PagerDefault')->limit(20);

  // Don't forget to tell the query object how to find the header information.
  $result = $query
    ->orderByHeader($header)
    // Get fields from {user_moderation}.
    ->fields('um', array('uid', 'upvotes', 'downvotes'))
    ->execute();

  // Save uid, upvotes, downvotes, operations, and selected field info
  // in $rows array for table.
  $rows = array();
  foreach ($result as $row) {
    // Get the user's uid and load the user object.
    $uid = $row->uid;
    $account = user_load($uid);
    // Get the selected user profile field for that user.
    $field_info = field_get_items('user', $account, $field_name);
    // @todo this is a problem if translated - should be a better way to do this
    $field_info_value = $field_info[0]['value'];

    $rows[] = array(
      $row->uid,
      $row->upvotes,
      $row->downvotes,
      // These go under the operations header which spans 2 columns.
      l(t('upvote'), 'user-moderation/' . $row->uid . '/upvote/nojs', array('attributes' => array('class' => 'use-ajax'))),
      l(t('downvote'), 'user-moderation/' . $row->uid . '/downvote/nojs', array('attributes' => array('class' => 'use-ajax'))),
      // This is the field that is selected by the admin.
      $field_info_value,
      l(t('edit'), 'user/' . $row->uid . '/edit'),
    );
  }

  // Build table.
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No users in approval queue.'),
    )
  );
  // Add the pager.
  $output .= theme('pager');

  return $output;
}
